# Class Assignment 5 - part 1 Report


## 1. Analysis, Design and Implementation

## 1.1 Analysis 

In this class, it was studied Continuous Integration (CI) and Continuous Delivery (CD).

Continuous integration refers to an automation process that developers use to build, test, and merge application code changes.
Continuous delivery is the next step in the process where the tested code from continuous integration is automatically deployed in various environments by a manual trigger.

On the image below is represented both steps during the development and delivery stages.


![ci-cl.png](ci-cl.png)

source: https://medium.com/@ranikamadurawe/continuous-integration-continuous-delivery-an-introduction-c7d40dfe9e06

The CI/CD tool used on this class is Jenkins. It offers a simple way to set up a continuous integration or continuous delivery (CI/CD) environment for almost any combination of languages and source code repositories using pipelines, as well as automating other routine development tasks.

Jenkins is one of the top DevOps tools because it is free, open-source and modular, and can integrate with pretty much every other DevOps tool out there. There are over a thousand plugins that you can use to extend Jenkins’ capabilities and make it more user-specific. 
  


 ## 1.2 Implementation

After the installation of the Jenkins and suggested plugins, it was necessary to indicate the administrator accounts.

To this implementation it was used the chat application form previous classes. The implementation of the pipeline was made using the Jenkins file script in the repository.

### 1.2.1 Creating new pipeline 

After sign in on the jenkins, initialize a new project indicating next that is will be a pipeline.

![newItem.png](newItem.png)


![pipeline.png](pipeline.png)


### 1.2.2 Indicating the pretended jenkins file

It was used a pipeline script from SCM, so we need to indicate the repository where the script is located.


![pipelineFromSCM.png](pipelineFromSCM.png)


![jenkinsFileLocation.png](jenkinsFileLocation.png)



### 1.2.2 Analyzing the script
### 1.2.2.1 Stage checkout

On this stage is indicated the repository where the app is present.


    stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://bitbucket.org/jsoares04/devops-21-22-atb-1040817'
                }
            }

### 1.2.2.2 Stage assemble

On this stage is indicated the directory of the repository where the application is present. Next, the SO system is identified and the build order is given.

    stage('Assemble')
            {
                steps {
                    echo 'Assembling...'
                    dir ("CA2/Part1") {
                        script {
                        if (isUnix()){
                            sh 'chmod +x gradlew'
                            sh './gradlew assemble'}
                        else
                            bat './gradlew assemble'
                    }
                    }
                }
            }

### 1.2.2.3 Stage test

On this step the order to run the app tests is given. Is indicated also the command for the creation of a xml file with the tests results.


    stage('Test') {
                steps {
                    echo 'testing...'
                    dir ("CA2/Part1"){
                        script {
                        if (isUnix()){
                            sh 'chmod +x gradlew'
                            sh './gradlew test'
                            junit 'build/test-results/test/*.xml'}
                        else
                        {
                            bat './gradlew test'
                            junit 'build/test-results/test/*.xml'}
                    }
                    }
                }
            }

### 1.2.2.4 Stage archive

The final stage is the archiving of the artifacts, its indicated the folder of the build files (or any other intended files).

    stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    dir ("CA2/Part1"){
                        archiveArtifacts 'build/distributions/*'
                    }
        }
        }

### 1.2.3 Build successfully

General view of project page


![buildSuccess.png](buildSuccess.png)

Artifacts saved

![artifacts.png](artifacts.png)



