# Class Assignment 5 - part 2 Report


## 1. Analysis, Design and Implementation

## 1.1 Analysis 

In this class, it was studied Continuous Integration (CI) and Continuous Delivery (CD) using Jenkins.

It was intended to create a pipeline, using the spring app form the class nr 3. 

REgarding part 1, is was implemented new stages such as:

* publish Javadocs in HTML files
* build and push docker image

 ## 1.2 Implementation

To publish javadocs and push docker images it was necessary no install new plugins on Jenkins:

* Javadoc
* HTML publisher
* Docker pipeline


### 1.2.1 Creating new pipeline 

After sign in on the jenkins, initialize a new project indicating next that is will be a pipeline.

![part2Pipeline.png](part2Pipeline.png)


### 1.2.2 Indicating the pretended jenkins file

It was used a pipeline script from SCM, so we need to indicate the repository where the script is located.

![pipelineConfig.png](pipelineConfig.png)

### 1.2.2 Analyzing the script - new stages
### 1.2.2.1 Stage javadoc

On this stage is indicated that Javadoc from the project should be collected.


    stage('JavaDoc') {
                        steps {
                            echo 'documenting...'
                            dir ("ca5/part2/react-and-spring-data-rest-basic"){
                                javadoc javadocDir: 'build/reports/tests/test', keepAll: false
                                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/reports/tests/test', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: 'Docs Loadtest Dashboard'])
                            }
                        }
                    }

### 1.2.2.2 Stage html

On this stage the Javadoc collected will be agregated on the index.html file.

    stage('HTML') {
                        steps {
                            echo 'html...'
                            dir ("ca5/part2/react-and-spring-data-rest-basic"){
                                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/reports/tests/test', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: 'Docs Loadtest Dashboard'])
                            }
                        }
                    }

Below the image of the generated hmtl:

![htmlReport.png](htmlReport.png)



### 1.2.2.3 Build and push Docker image

To push a docker image its necessary to create jenkins credentials with the DockerHub login data.

![dockerHubCredentials.png](dockerHubCredentials.png)

Its necessary to indicate on the docker.withResgistry  command the id of the credentials to use.


    stage ('Building Docker Image') {
                    steps {
                        
                        script {
                            docker.withRegistry('','DockerHUB'){
                                def app =  docker.build("azemeis/ca5","ca5/part2/react-and-spring-data-rest-basic")
                            app.push()
                                
                            }
                        }
                    }
            }

Below the image present on the DockerHub

![dockerHubPush.png](dockerHubPush.png)

### 1.2.3 Build successfully

General view of project page


![success.png](success.png)

Artifacts saved

![artifacts.png](artifacts.png)

## 2. Analysis of an Alternative - Buddy

Buddy is a DevOps automation platform that allows continuous integration, continuous deployment and feedback. This tool was made for working with projects that use code from the Bitbucket and GitHub repositories. Buddy is a commercial tool with a straightforward, user-friendly interface and minimalistic material design. This customer-focused solution has 24/7 live representative support, and its enterprise version allows self-hosting it on a server.

* Instinctive UI
* Intuitive deployment flow building
* Docker support
* Available presets and hints
* Offers advanced automation while requiring basic knowledge
* Ability to make corrections to the developed code
* Flexible automation, cloning, variables, connections and notifications.
  
In resume, Buddy is a modern and intuitive CI tool. By keeping the interface simple Buddy makes the user experience the top priority and the time you need to invest in learning the tool to the minimum.

source: https://smartbear.com/blog/top-continuous-integration-tools-for-devops/

## 2.1 Jenkins vs Buddy

On the tables below are indicated the main diferences between both tools.

First the technical diferences:

![jenkinsVSbuddy.png](jenkinsVSbuddy.png)

And tactical:

![jenkinsVSbuddy2.png](jenkinsVSbuddy2.png)

source: https://hackernoon.com/buddy-vs-jenkins-mt4v32u2


## 2.2 Implementation

In this chapter it's illustrated the implementation of a pipeline using buddy, regarding the same application and functionalities form the class.

It was used the Wizard, but its possible to build the script entirely. 


*&emsp; Buddy dashboard*

![buddyDashboard.png](buddyDashboard.png)

*&emsp; Buddy assemble*

![buddyAssemble.png](buddyAssemble.png)

*&emsp; Buddy test*

![buddyTest.png](buddyTest.png)

*&emsp; Buddy build Docker*

![buddyBuildDocker.png](buddyBuildDocker.png)

*&emsp; Buddy push Docker*

![buddyPushDocker.png](buddyPushDocker.png)

*&emsp; Buddy push Docker*

![buddyPipeline.png](buddyPipeline.png)

Buddy features the creating of the script from the Wizard implementation.

*&emsp; Generated yaml script*

    pipeline: "Final"
    on: "CLICK"
    refs:
    - "refs/heads/master"
    priority: "NORMAL"
    fail_on_prepare_env_warning: true
    actions:
    - action: "Assemble"
        type: "BUILD"
        working_directory: "/buddy/devops-21-22-atb-1040817"
        docker_image_name: "library/gradle"
        docker_image_tag: "7.4.2"
        execute_commands:
        - "cd \"ca5/part2-alternative/react-and-spring-data-rest-basic\""
        - "sh ./gradlew assemble"
        - ""
        volume_mappings:
        - "/:/buddy/devops-21-22-atb-1040817"
        cache_base_image: true
        shell: "SH"
    - action: "Test"
        type: "BUILD"
        working_directory: "/buddy/devops-21-22-atb-1040817"
        docker_image_name: "library/gradle"
        docker_image_tag: "7.4.2"
        execute_commands:
        - "cd \"ca5/part2-alternative/react-and-spring-data-rest-basic\""
        - "sh ./gradlew test"
        volume_mappings:
        - "/:/buddy/devops-21-22-atb-1040817"
        cache_base_image: true
        shell: "SH"
    - action: "Build Docker image"
        type: "DOCKERFILE"
        docker_image_tag: "latest"
        dockerfile_path: "ca5/part2-alternative/react-and-spring-data-rest-basic/Dockerfile"
        context_path: "ca5/part2-alternative/react-and-spring-data-rest-basic"
        repository: "azemeis/ca5-alternative"
        target_platform: "linux/amd64"
        integration_hash: "4Jq3R9OjoQeZvoKRepZVg8lNDM"
    - action: "Push Docker image"
        type: "DOCKER_PUSH"
        docker_image_tag: "lateste"
        repository: "azemeis/ca5-alternative"
        integration_hash: "4Jq3R9OjoQeZvoKRepZVg8lNDM"
    - action: "Archive directory"
        type: "ZIP"
        local_path: "ca5/part2-alternative/react-and-spring-data-rest-basic/build/libs"
        destination: "ca5/part2-alternative/react-and-spring-data-rest-basic/build/ca5/part2-alternative/react-and-spring-data-rest-basic/build/libs.zip"
        deployment_excludes:
        - ".git"