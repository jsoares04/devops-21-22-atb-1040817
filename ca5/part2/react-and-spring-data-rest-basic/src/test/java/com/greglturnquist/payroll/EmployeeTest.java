package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void emptyStringFirstName() {

        //Arrange
        String firstName = "";
        String lastName = "Baggins";
        String description = "Ring bearer";



        //Act & assert
        Employee employeeResult = new Employee(firstName, lastName, description);

        assertEquals(employeeResult,employeeResult);
    }



}