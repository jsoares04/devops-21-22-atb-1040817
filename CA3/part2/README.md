# Class Assignment 3 - part 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA3 - part2 is described on
the next points. 

The goal of the part 2 of the class is the implementation of virtual 
machines using the tool Vagrant. Vagrant is a tool for building and 
managing virtual machine environments in a single workflow. With an 
easy-to-use workflow and focus on automation, Vagrant lowers 
development environment setup time, increases production parity, and 
makes the "works on my machine" excuse a relic of the past.

The first contact with Vagrant was following training steps indicated 
on the file *devops06.pdf*. An initialization using the indicated box,
installing the apache, setting up the network ip where some tasks
executed.

## 1.1 **Implementation**

In an initial solution it was used the vagrant file present on the 
repository *https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/*. This Vagrant
file is used to create and provision 2 VMs:
* web: this VM is used to run tomcat and the spring boot basic 
application


  ![webServerInit](webServerInit.PNG)

* db: this VM is used to execute the H2 server database

 ![dataBaseInit](dataBaseInit.PNG)

### 1.1.1 *react-and-spring-data-rest-basic*

To achieve the goal of running the spring application, it was necessary 
to modify the path for the repository.

*&emsp;before:*

![vagrantBefore](vagrantBefore.PNG)

*&emsp; after:*

![vagrantAfter](vagrantAfter.PNG)

After the modification, using the command Vagrant UP both VM where started,
using virtual box, and it was possible to run the spring application.

*&emsp; terminal information:*
 


    User@Laptop-Ze MINGW64 ~/OneDrive - Instituto Superior de Engenharia do Porto/Documents/SWitCH/DEVOPS/devops-21-22-atb-1040817/CA3/part2 (master)
    $ vagrant up
    Bringing machine 'db' up with 'virtualbox' provider...
    Bringing machine 'web' up with 'virtualbox' provider...
    ==> db: Checking if box 'envimation/ubuntu-xenial' version '1.0.3-1516241473' is up to date...
    ==> db: Clearing any previously set forwarded ports...
    ==> db: Clearing any previously set network interfaces...
    ==> db: Preparing network interfaces based on configuration...
    db: Adapter 1: nat     
    db: Adapter 2: hostonly
    ==> db: Forwarding ports...
    db: 8082 (guest) => 8082 (host) (adapter 1)
    db: 9092 (guest) => 9092 (host) (adapter 1)
    db: 22 (guest) => 2222 (host) (adapter 1)  
    ==> db: Booting VM...
    ==> db: Waiting for machine to boot. This may take a few minutes...
    db: SSH address: 127.0.0.1:2222
    db: SSH username: vagrant
    db: SSH auth method: private key
    db: Warning: Connection reset. Retrying...
    db: Warning: Remote connection disconnect. Retrying...
    ==> db: Machine booted and ready!
    ==> db: Checking for guest additions in VM...
    db: The guest additions on this VM do not match the installed version of   
    db: VirtualBox! In most cases this is fine, but in rare cases it can       
    db: prevent things such as shared folders from working properly. If you see
    db: shared folder errors, please make sure the guest additions within the  
    db: virtual machine match the version of VirtualBox you have installed on
    db: your host and reload your VM.
    db:
    db: Guest Additions Version: 5.1.30
    db: VirtualBox Version: 6.1
    ==> db: Setting hostname...
    ==> db: Configuring and enabling network interfaces...
    ==> db: Mounting shared folders...
    db: /vagrant => C:/Users/User/OneDrive - Instituto Superior de Engenharia do Porto/Documents/SWitCH/DEVOPS/devops-21-22-atb-1040817/CA3/part2
    ==> db: Machine already provisioned. Run `vagrant provision` or use the `--provision`
    ==> db: flag to force provisioning. Provisioners marked to run always will still run.
    ==> db: Running provisioner: shell...
    db: Running: inline script
    ==> web: Checking if box 'envimation/ubuntu-xenial' version '1.0.3-1516241473' is up to date...
    ==> web: Resuming suspended VM...
    ==> web: Booting VM...
    ==> web: Waiting for machine to boot. This may take a few minutes...
    web: SSH address: 127.0.0.1:2200
    web: SSH username: vagrant
    web: SSH auth method: private key
    ==> web: Machine booted and ready!
    ==> web: Machine already provisioned. Run `vagrant provision` or use the `--provision`
    ==> web: flag to force provisioning. Provisioners marked to run always will still run.

*&emsp; Virtual Box PrintScreen:*

![virtualBoxPrint](virtualBoxPrint.PNG)

*&emsp;application response after accessing http://localhost:8080/basic-0.0.1-SNAPSHOT/*

![apprPrint](appPrint.PNG)


*&emsp;H2 console*

![h2Console](h2Console.PNG)

![h2ConsoleInto](h2ConsoleInto.PNG)

## 2. Analysis of an Alternative

Hypervisor is a software that allows you to run one or multiple virtual
machines with their own operating systems (guest operating systems) on
a physical computer, which is called a host machine.

There are two types of hypervisors:

* Type 1: hypervisors run directly on the system hardware –
  A “bare metal” embedded hypervisor, for example HyperV.
* Type 2: hypervisors run on a host operating system that provides
  virtualization services, such as I/O device support and memory management.

### 2.1 VirtualBox vs VMware



VirtualBox and VMware workstation are a type 2 hypervisor that are 
also called as  hosted hypervisor. A type 2 hypervisor is an application 
that runs on the operating system (OS). When a physical computer starts,
the operating system installed on the host loads normally and the 
hypervisor is inactive. A user starts the hypervisor application and 
then starts the needed virtual machines and VM hosted processes are 
created.

Some differences between VirtualBox and VWware are indicated on the 
table below:

|     | VirtualBox | VMware |
|-----|------------|--------|
|  Durability of the environment   |    Can be slow in production or testing environment.        |   Swift in utilizing the resources of the host machine.      |
| User Friendly and time saver    | Simple and user friendly interface.           |    Slightly complicated user interface when compared to VirtualBox.   |
|    Target Audience |       Suitable for developers, testers, students and home use.    | Can be complicated if end user is not a system engineer.       |
|  Hardware and software virtualisation   | Provides virtualization for both hardware and software         |   Provides virtualization only for hardware.      |


### 2.2 Implementation of the Alternative

As a start it was used the same Spring project and the same Vagrant 
file from the previous chapter. There were necessary modifications in 
both.

### 2.2.1 modifications on Vagrantfile

* Modify the ip

&emsp;The ips used previously when implementing with VirtualBox are
&emsp;occupied, so it's necessary to choose free ips for the new VM.
This update needs to be done in both VM (web and DB).

*&emsp;DB*

![ipConfigAlternative](ipConfigAlternative.png)

*&emsp;Web*

![ipConfigWebAlternative](ipConfigWebAlternative.png)

* Memory increase

&emsp;After some problems starting the WEB VM, it was figured out that
the memory RAM allocated wasn't enough so it was indicated the new 
size on the vagrant file.

![memoryIncrease](memoryIncrease.png)

### 2.2.2 modifications on Spring application

* H2 matching ip

&emsp; It was necessary to match the ip for the application H2 database
with the one used on the DB VM. That update was done on the 
application.properties file.

![ipUpdateAppAlternative](ipUpdateAppAlternative.png)

### 2.2.3 mounting the VM's

* To use VMware with vagrant is necessary to install a plugin.

>$ vagrant plugin install vagrant-vmware-desktop
> 
>Installing the 'vagrant-vmware-desktop' plugin. This can take a few minutes...
> 
>Waiting for cleanup before exiting...

* to force Vagrant to use the VMware as a provider it was 
necessary to use the command below.

>vagrant up --provider=vmware_workstation

### 2.2.4 images of the success case of the alternative implementation
implementation.

*&emsp;vagrant up with VMware as provider*

![vagrantUpVMware](vagrantUpVMware.PNG)

*&emsp;last lines of the VM initialization*

![vagrantUpVMwareConclusion](vagrantUpVMwareConclusion.PNG)

*&emsp;two VMs mounted on the VMware application*

![VMwarePrintAlternative](VMwarePrintAlternative.PNG)

*&emsp;application on the browser*

![appPrintAlternative](appPrintAlternative.PNG)

*&emsp;H2 connection screen*

![h2ConsoleAlternative](h2ConsoleAlternative.PNG)

*&emsp;H2 connected screen*

![h2ConsoleIntoAlternative](h2ConsoleIntoAlternative.PNG)



