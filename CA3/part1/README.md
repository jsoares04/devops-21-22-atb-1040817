# Class Assignment 3 - part 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA3 - part1 is described on
the next points. 

The starting point of the exercise was the implementation of a virtual
machine using Oracle VirtualBox. It was necessary to follow the steps 
present on the ppt presentation *devops05.pdf*, with the goal of 
guarantee the conditions to run applications through the virtual machine.

For this exercise a previous repository, already shared with the teacher,
was used. The intention is to build and run applications contained on 
the repository, in particular spring boot tutorial basic project and the
gradle_basic_demo project.

## 1.1 **Implementation**

After cloning the repository to the virtual machine, building and 
running the applications was the next step.

### 1.1.1 *gradle_basic_demo*

After moving to the application folder, and trying to build the project
there was a error as indicated below:
>$ ./gradlew build
> 
>build error

The problem was that the file *gradle-wrapper.jar* wasn't on the repository because that extension is contemplated on the *gitIgnore*.
The file is necessary to run the application, so the commit of the 
file was forced and pushed to the repository.

After updating the repository on the VM, building was 
possible.

![BuildingSuccessful](BuildingSuccessful.PNG)

Then its possible to run the chat server using the task runServer, implemented on the previous class:


![runServer](runServer.PNG)

Taking into account that our virtual machine doesn't have an interface,
to run the chat client we should use the host machine. To do that, 
it's necessary to change the arguments on the gradle task in order to 
point to the virtual machine port instead of localhost.

*&emsp; Previous task:*

![runClientLocalhost](runClientLocalhost.PNG)

*&emsp; Modified task:*


![runClientVMip](runClientVMip.PNG)

*&emsp; Client running:*

![runClient](runClient.PNG)

*&emsp; Chat interface:*

![runClientUserName](runClientUserName.PNG)

*&emsp; Indication of new user on the VM:*

![runClientUserJoin](runClientUserJoin.PNG)


### 1.1.2 *spring boot tutorial*

There wasn't any problem building and running the application 
in the VM.

*&emsp; build*


![build](build.PNG)

*&emsp; bui 2nd page*

![build2](build2.PNG)

Tho check if the app is correctly running we should use the browser of
the host machine, but instead of using the address 
*https://localhost:8080* we use *https://{ip of the VM}:8080*.

![browser](browser.PNG)
