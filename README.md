# Individual Repository for DevOps

This repository contains the files for all the class assignemts of DevOps.

* [Class Assignment 1](https://bitbucket.org/jsoares04/devops-21-22-atb-1040817/src/master/CA1/)
* [Class Assignment 2 - part1](https://bitbucket.org/jsoares04/devops-21-22-atb-1040817/src/master/CA2/Part1)
* [Class Assignment 2 - part2](https://bitbucket.org/jsoares04/devops-21-22-atb-1040817/src/master/CA2/Part2)
* [Class Assignment 3 - part1](https://bitbucket.org/jsoares04/devops-21-22-atb-1040817/src/master/CA3/part1)
* [Class Assignment 3 - part2](https://bitbucket.org/jsoares04/devops-21-22-atb-1040817/src/master/CA3/part2)
