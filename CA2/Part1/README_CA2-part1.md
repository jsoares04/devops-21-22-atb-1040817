# Class Assignment 2 - part 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA2 - part1 is described on the next points. It's also presented an alternative and the implementation of the chosen alternative.

For this exercise a previous repository, already shared with the teacher,
was used. The project used chat application, some modifications, 
debugging and creation of new files where made, with the intention of 
simulating an app development environment.

## 1.1 **Implementation**

After downloading the chat application and placing it on the correct folder, the implementation and features where looked up trying to understand the working process of the application.

The new folder was committed to the class repository and also an Issue was created referencing the part 1 of the exercise.

###1.1.1 *Add a new task to execute the server.*

To solve the problem, it was taking into account the task runClient present on the file build.gradle.

The code added to build.gradle is represented below, basic the task will execute the class ChatServerApp with the argument '59001', that represents the por number of the server to be initialized.

> task runServer(type:JavaExec, dependsOn: classes){
> 
>group = "DevOps"
> 
>description = "Launches a server on localhost:59001 "
>
>    classpath = sourceSets.main.runtimeClasspath
>
>    mainClass = 'basic_demo.ChatServerApp'
>
>    args  '59001'
> 
>}

###1.1.2 *Add a simple unit test and update the gradle script so that it is able to execute the test.*

Folder '/src/test/java/basic_demo' was created and the file AppTest.java with the code indicated on the exercise was created.

If the JUnit dependency wouldn't be added, the build won't perform. 

So the dependency was indicated on build.gradle as represented below.


>dependencies {
> 
> ...
> 
> testImplementation 'junit:junit:4.13'
>
>} 

###1.1.3 *Add a new task of type Copy to be used to make a backup of the sources of the application.*


To solve the exercise, a simple task with the type: Copy was implemented on the build.gradle as indicated below.
> task copyDocs(type: Copy) {
> 
>from 'src'
> 
>into 'backup'
> 
>}
>
###1.1.4 *Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application.*

Also, a simple task was used, similar with the previous but the type is ZIP instead of Copy.



> task zip(type:Zip){
> 
>from 'src'
> 
> into 'backup.zip'
> 
>destinationDir(file('backupZip'))
> 
> }

##1.2 *Notes*

During the implementation several commits were made, referencing 
the issue #4. The tag CA1-part1 was applied when the implementation 
had been completed.

![LastCommit](/home/josesoares/Documents/SWitCH/DEVOPS/DEVOPS_REPO/DEVOPS_REPO/devops-21-22-atb-1040817/CA2/Part1/LastCommit.png)
