package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    void testConstructorEmptyStrings() {
        //Arrange
        //ACT
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee("", "soares",
                                "teste", "calceteiro",
                                2, "jos_soares@hotmail.com"));
        //Assert
        assertEquals("Data can't be empty or null",
                exception.getMessage());
    }

    @Test
    void testConstructorNullStrings() {
        //Arrange
        //ACT
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee(null, "soares",
                                null, "calceteiro",
                                2, "jos_soares@hotmail.com"));
        //Assert
        assertEquals("Data can't be empty or null",
                exception.getMessage());
    }

    @Test
    void emailInvalid() {
        //Arrange
        //ACT
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee("sds", "soares",
                                "wawa", "calceteiro",
                                2, "jos_soareshotmail.com"));
        //Assert
        assertEquals("Email incorrect",
                exception.getMessage());
    }

}