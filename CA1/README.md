# Class Assignment 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA1 is described on the next points. It's also presented an alternative and the implementation of the chosen alternative.

For this exercise a previous repository, already shared with the teacher,
was used. The project used was a Spring tutorial, some modifications, 
debugging and creation of new files where made, with the intention of 
simulating an app development environment.

## 1.1 **CA1-part1**


Using a terminal on the repository folder, we should do the steps
indicated below.

This command show the status of our Git repository. The folder CA1 was
created after the last commit, so its indicated that we need to track
the new files and folders.
> *$ git status*
>
>On branch master
>
>Your branch is up to date with 'origin/master'.
>
>Untracked files:
>
>(use "git add <file>..." to include in what will be committed)
>
>CA1/
>
>nothing added to commit but untracked files present (use "git add" to track)

This command add all the files that where untracked.


> *$ git add .*

With this command we will apply a commit with the message indicated.

> *$ git commit -a -m "CA1 first commit"*

Command to the creation of a TAG and the respective message.

> *$ git tag -a v1.1.0 -m "Initial version"*

Command to verify the available tag's.

> *$ git tag*
>
>v1.1.0

Command to push to the repository the open commits.

> *$ git push origin master*
>
Command to apply the tag to the last push.

> *$ git push origin v1.1.0*
>

After applying the modifications on the project and the test creation
on the project, new files and folders where created.

Because new files and folders where added its necessary to add
untracked files.

> *$ git add .*
>
With this command we will apply a commit with the message indicated.
> *$ git commit -m "CA2 second commit"*

Command to push to the repository the open commits.

> *$ git push origin master*
>
Command to the creation of the new TAGs and the respective message.

> *$ git tag -a v.1.2.2 -m "new feature is completed and tested"*
>
>*$ git tag -a CA1-part1 -m "new feature is completed and tested"*

Command to verify the available tag's.

> *$ git tag*
>
>v.1.2.2
>
>v1.1.0
>
> CA1-part1

Command to apply the tag to the last push.

> *$ git push origin v1.2.2*

New commit to close the issue #1

> *git commit -a -m "close #1; closing the issue"*
>
Command to apply the tag to the last push.

> *$ git push origin CA1-part1*
>

## 1.2 **CA1-part2**

> *$ git branch email-field*
>
Command to create a new branch.

> *$ git branch*
>
>email-field
>
> *master
>
Command to show the available branches. The * before indicates the
active branch.

> *$ git checkout email-field*
>
Command to change the active branch.

The modifications to the project where applied as indicated on the
Statement.

> *$ git commit -a -m "first branch commit - email field added and tested - references #2"*
>
Command we will apply a commit with the message indicated.

> *$ git push origin email-field*
>
Command to push to the repository the open commits to the branch
email-field.

> *$ git tag -a v1.3.0 -m "my version 1.3.0"*
>
Creation of the new TAG and the respective message.

> *$ git checkout master*
>
Change the active branch to master.

> *$ git merge email-field*
>
Command to merge the branch email-field. Modifications made on the
branch email-field will be applied on the branch master.

> *$ git push origin v1.3.0*
>
Apply the tag on the last push.

> *$ git branch fix-invalid-email*
>
Create a new branch

> *$ git checkout fix-invalid-email*
>
Change the active branch to fix-invalid-email.

Modifications to the project applied as indicated on the Statement.

Commit the changes and also reference the commit to the issue number


> *$ git commit -a -m "Email validation added - references #2"*
>

Commit changes, new tests.

> *$ git commit -a -m "Added test to verify the email exception - references #2"*
>

Change active branch and merge the changes.

> *$ git checkout master*
>
>*$ git merge fix-invalid-email*
>
Push the commits.

> *$ git push  origin master*
>
Create new tags.

> *$ git tag -a CA1-part2 -m "CA1-part2 closed"*
>
Apply the tag to the last commit.

> *$ git push  origin CA1-part2*
>

## 2. Analysis of an Alternative

The alternative chosen to compare was Mercurial for the version
control and Helix Team Hub for the repository.

### 2.1 Git vs Mercurial

The main difference between the Version Controls are indicaded ou the table below.

| GIT                                                                                                       | Mercurial                                                          | 
|:----------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------|
| Git is a little bit of complex than Mercurial.                                                            | Mercurial is simpler than Git.                                     |                          
| No VCS are entirely secured, but Git offers many functions to enhance safety.                             | Mercurial may be safer for fresher. It has more security features. |                          
| Git has a powerful and effective branching model. Branching in Git is better than Branching in Mercurial. | Branching in Mercurial doesn't refer the same meaning as in Git.   |
|Git supports the staging area, which is known as the index file.| There is no index or staging area before the commit in Mercurial. |
|The most significant benefit with Git is that it has become an industry-standard, which means more developers are familiar with it.| Mercurial's significant benefit is that it's easy to learn and use, which is useful for less-technical content contributors. |
|Git needs periodic maintenance for repositories.| It does not require any maintenance. |
|It holds Linux heritage.| It is python based. |
|Git is slightly slower than Mercurial.| It is faster than Git.|
|Git supports the unlimited number of parents.| Mercurial allows only two parents.|
*source:https://www.javatpoint.com/git-vs-mercurial*

In the small interaction with Mercurial, I can confirm that it is easy to use, It's also fast.

### 2.2 Helix team hub

Because bitbucket doesn't support Mercurial, the alternative choice was Helix Team Hub.
It's similar with bitbucket, but UI interface seems more confuse and less user-friendly.

## 3. Implementation of the Alternative

### 3.1 Install Mercurial

In Linux UBUNTU, it was used the terminal to install:

> *sudo apt-get install mercurial

### 3.2 Registration on the HELIX TEAM HUB

The registration was made online and a repository with the name *devops-21-22-atb-1040817* was created.

### 3.3 Steps

To the implementation, instead of using the React & Spring
application, it was simulated the steps using simple Markdown files.

* CA1-part1 alternative

Create a clone from the repo to the local machine:

> *$ hg clone https://JSoares@helixteamhub.cloud/wild-jar-9431/projects/devops-21-22-atb-1040817/repositories/mercurial/devops-21-22-atb-1040817*
>
The file CA1-part1-1stFile.md was added to the directory. To track the
new file it was used the command below.

> *$ hg add*
>

Commit the new file to the repo.

> *$ hg commit -m "First Commit"*
>
Push the commit to the repository
> *$ hg push*

Create a new TAG.
> *$ hg tag v1.1.0*
>
Push the TAG to the last commit
> *$ hg push*

The first file was modified and created a new one. Track the new file,
commit and push the commit.
> *$ hg add*
>
Commit
> *$ hg commit -m "Second Commit"*
>
Pushing
> *$ hg push*
>
Create a new TAG.
> *$ hg tag v1.2.0*
>
Push the TAG to the last commit
> *$ hg push*
>
and the last TAG Create a new TAG.
> *$ hg tag ca1-part1*
>
Push the TAG to the last commit
> *$ hg push*
>

* CA1-part2 alternative

Create the branch email-field.
> *$ hg branch email-field*
>
Made some modifications on the 1st commit file and commit and push the
changes.
> *$ hg commit -m "first commit on the branch"*
>
>*$ hg push*
>
Activate the default branch, merge the changes commit and push.
> *$ hg update default*
>
> *$ hg merge email-field*
>
> *$ hg commit -m "Commit after merge"*
>
> *$ hg push*



Create a new branch, modify the files and merge the branches
> *$ hg branch fix-invalid-email*
>
>*$ hg commit -m "first commit on the branch fix-email-field"*
>
>*$ hg push*
>
>*$ hg update default*
>
>*$ hg merge fix-invalid-email*

Create a new TAG.
> *$ hg tag v1.3.1*
>
Push the TAG to the last commit
> *$ hg push*
>
Create a new TAG.
> *$ hg tag CA1-part2*
>
Push the TAG to the last commit
> *$ hg push*
>
Below the image of the last commits on the Helix Team Hub Repo

![PrintScreenLastCommit](PrintScreenLastCommit.png)





