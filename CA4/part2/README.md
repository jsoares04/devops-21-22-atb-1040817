# Class Assignment 4 - part 2 Report


## 1. Analysis, Design and Implementation

The need of defining and share a multi-container applications, was one of the reasons that led to the creation of docker compose.

Docker Compose is a tool for defining and running multi-container Docker applications.

With Compose, we can create a YAML file to define the services and with a single command, can spin everything up or tear it all down.


## 1.1 Implementation 

To use docker-compose.ynl we still need the docker files for the  containers that are intended to use. The port forwarding, ips and subnets are declared on the Docker-compose.

## 1.1.1 Mounting the two containers


*&emsp; Docker-compose.ynl*

    version: '3'
    services:
    web:
        build: web
        ports:
        - "8080:8080"
        networks:
        default:
            ipv4_address: 192.168.33.10
        depends_on:
        - "db"
    db:
        build: db
        ports:
        - "8082:8082"
        - "9092:9092"
        volumes:
        - ./data:/usr/src/data-backup
        networks:
        default:
            ipv4_address: 192.168.33.11
    networks:
    default:
        ipam:
        driver: default
        config:
            - subnet: 192.168.33.0/24

*&emsp; Dockerfile web container*


    FROM tomcat:9.0.48-jdk11-openjdk-slim

    RUN apt-get update -y

    RUN apt-get install -f

    RUN apt-get install git -y

    RUN apt-get install nodejs -y

    RUN apt-get install npm -y

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build/

    RUN git clone https://JSoares04@bitbucket.org/jsoares04/devops-21-22-atb-1040817.git

    WORKDIR /tmp/build/devops-21-22-atb-1040817/CA4/part2/react-and-spring-data-rest-basic

    RUN chmod u+x gradlew

    RUN ./gradlew clean build

    RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

*&emsp; Dockerfile db container*

    FROM ubuntu

    RUN apt-get update && \
    apt-get install -y openjdk-8-jdk-headless && \
    apt-get install unzip -y && \
    apt-get install wget -y

    RUN mkdir -p /usr/src/app

    WORKDIR /usr/src/app/

    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

    EXPOSE 8082
    EXPOSE 9092

    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists


*&emsp; docker-compose up*

    $ sudo docker-compose up
    part2_db_1 is up-to-date
    part2_web_1 is up-to-date
    Attaching to part2_db_1, part2_web_1
    web_1  | NOTE: Picked up JDK_JAVA_OPTIONS:  --add-opens=java.base/java.lang=ALL-UNNAMED --add-opens=java.base/java.io=ALL-UNNAMED --add-opens=java.base/java.util=ALL-UNNAMED --add-opens=java.base/java.util.concurrent=ALL-UNNAMED --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED
    web_1  | 19-May-2022 10:30:37.809 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version name:   Apache Tomcat/9.0.48
    web_1  | 19-May-2022 10:30:37.813 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server built:          Jun 10 2021 09:22:01 UTC
    web_1  | 19-May-2022 10:30:37.814 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version number: 9.0.48.0
    web_1  | 19-May-2022 10:30:37.814 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Name:               Linux
    web_1  | 19-May-2022 10:30:37.814 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Version:            5.16.12-051612-generic
    web_1  | 19-May-2022 10:30:37.815 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Architecture:          amd64
    web_1  | 19-May-2022 10:30:37.815 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Java Home:             /usr/local/openjdk-11
    web_1  | 19-May-2022 10:30:37.815 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Version:           11.0.11+9
    web_1  | 19-May-2022 10:30:37.816 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Vendor:            Oracle Corporation
    web_1  | 19-May-2022 10:30:37.816 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /usr/local/tomcat
    web_1  | 19-May-2022 10:30:37.816 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /usr/local/tomcat
    web_1  | 19-May-2022 10:30:37.834 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.base/java.lang=ALL-UNNAMED
    web_1  | 19-May-2022 10:30:37.835 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.base/java.io=ALL-UNNAMED

    (.........)

    web_1  | 2022-05-19 10:38:30.627  INFO 1 --- [           main] org.hibernate.Version                    : HHH000412: Hibernate ORM core version 5.4.12.Final
    web_1  | 2022-05-19 10:38:30.910  INFO 1 --- [           main] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.1.0.Final}
    web_1  | 2022-05-19 10:38:31.144  INFO 1 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.H2Dialect
    web_1  | 2022-05-19 10:38:32.396  INFO 1 --- [           main] o.h.e.t.j.p.i.JtaPlatformInitiator       : HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
    web_1  | 2022-05-19 10:38:32.408  INFO 1 --- [           main] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
    web_1  | 2022-05-19 10:38:33.762  WARN 1 --- [           main] JpaBaseConfiguration$JpaWebConfiguration : spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
    web_1  | 2022-05-19 10:38:34.104  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
    web_1  | 2022-05-19 10:38:34.317  INFO 1 --- [           main] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page template: index
    web_1  | 2022-05-19 10:38:35.201  INFO 1 --- [           main] c.g.payroll.ServletInitializer           : Started ServletInitializer in 8.873 seconds (JVM running for 13.248)
    web_1  | 19-May-2022 10:38:35.385 INFO [main] org.apache.catalina.startup.HostConfig.deployWAR Deployment of web application archive [/usr/local/tomcat/webapps/basic-0.0.1-SNAPSHOT.war] has finished in [12,018] ms
    web_1  | 19-May-2022 10:38:35.389 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
    web_1  | 19-May-2022 10:38:35.402 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in [12156] milliseconds

*&emsp; Services running*

    $ sudo docker-compose ps
    Name                  Command               State                             Ports                          
    ----------------------------------------------------------------------------------------------------------------
    part2_db_1    /bin/sh -c java -cp ./h2-1 ...   Up      0.0.0.0:8082->8082/tcp,:::8082->8082/tcp,                
                                                        0.0.0.0:9092->9092/tcp,:::9092->9092/tcp                 
    part2_web_1   catalina.sh run                  Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp

*&emsp; Application running*

![appRunning](appRunning.png)


*&emsp; H2 connection*

![h2Connection](h2Connection.png)


*&emsp; H2 connected*

![h2Connected](h2Connected.png)

## 1.1.2 Copy the volume

Volumes are the preferred mechanism for persisting data generated by and used by Docker containers. While bind mounts are dependent on the directory structure and OS of the host machine, volumes are completely managed by Docker.

To use a volumes its necessary to indicates on the docker-compose.ynl, and define the paths of the host folder and container folder. 

*&emsp; Docker-compose volume declaration*

        volumes:
        - ./data:/usr/src/data-backup
     

To copy the db file to the volume path, its necessary to connect to container through ssh and copy the file using the copy command on the terminal.

*&emsp; SSH command to copy the database*

    $ sudo docker-compose exec d
    b sh
    [sudo] password for josesoares: 
    # pwd
    /usr/src/app
    # ls
    h2-1.4.200.jar	jpadb.mv.db
    # cp /usr/cp usr/src/app/jpadb.mv.db /usr/src/data-backup
    # cp /usr/src/app/jpadb.mv.db /usr/src/data-backup
    # pwd
    /usr/src/app
    # cd ..
    # cd data-backup
    # ls -l
    total 32
    -rw-r--r-- 1 root root 28672 May 19 15:37 jpadb.mv.db
    -rw-rw-r-- 1 1000 1000   103 May 16 08:43 readme.txt
    #

*&emsp; SSH command to copy the database*


![dataBaseCopy](dataBaseCopy.png)






### 1.2 Publishing the images to DockerHub

*&emsp; Docker images*


    sudo docker ps
    [sudo] password for josesoares: 
    CONTAINER ID   IMAGE       COMMAND                  CREATED          STATUS          PORTS                                                                                  NAMES
    fe53f6729757   part2_web   "catalina.sh run"        15 minutes ago   Up 15 minutes   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp                                              part2_web_1
    9e6a96df73a9   part2_db    "/bin/sh -c 'java -c…"   4 hours ago      Up 15 minutes   0.0.0.0:8082->8082/tcp, :::8082->8082/tcp, 0.0.0.0:9092->9092/tcp

*&emsp; Tag both images*

    $ sudo docker tag part2_web:latest azemeis/part2web:latest

    $ sudo docker tag part2_db:latest azemeis/part2db:latest

*&emsp; Push both images*

    $ sudo docker push azemeis/part2web:latest

    $ sudo docker push azemeis/part2db:latest

*&emsp; Docker Hub*

![dockerHub](dockerHub.png)

## 2. Analysis of an Alternative - Kubernetes

Kubernetes is open-source orchestration software that provides an API to control how and where these containers will run. It allows you to run Docker containers and workloads and helps to bypass some of the operational complexities inherent in scaling multiple containers deployed across multiple servers.

With Kubernetes, you can orchestrate a cluster of virtual machines and schedule containers to run on those virtual machines based on available compute resources and the resource requirements of each container. Containers are grouped into pods, the basic operating unit of Kubernetes. These containers and pods can scale to the desired state and you can manage their lifecycle to keep your applications operational.

![Docker-Kubernetes-together](Docker-Kubernetes-together.png)
*&emsp; Comparing traditional, virtualized, containerized and Kubernetes deployment architectures*

## 2.1 Docker vs Kubernetes

Talking about Kubernetes and Docker is normally reduce to comparing both, "should I use Kubernetes or Docker?". This approach isn't correct because they are comparing complementary services instead of direct competitors. Essentially, they are two different technologies that work well together for building, delivering, and scaling containerized applications.

You need a container runtime like Docker Engine  to start and stop containers on a host. when the number and complexity increases its necessary an orchestrator to manage things like: 
* Where will the next container start,
* How do you make a container highly available,
* How do you control which containers can communicate with other containers,
   
That’s where an orchestrator such as Kubernetes comes in.

## 2.2 Using Kubernetes as an alternative to the docker-compose

Its possible to mount and start the two containers ( web & db ) on a Kubernetes Container Pod. Pods are the smallest unit that can be deployed and managed by Kubernetes. 

The two types of Pods are:

* Single Container pods,
* Multi Container Pods.

A pod is a group of one or more containers that are deployed together on the same host. The pod is deployed with a shared storage/network, and a specification for how to run the containers.Containers can easily communicate with other containers in the same pod as though they are on the same machine.


![multi-container-pod-design](multi-container-pod-design.png)

*&emsp; Kubernetes pod representation*

There are three ways that containers in the pod communicate with each other. Namely,   and The communication between the containers in the same can be done in three diferent ways:

* Shared Network Namespace,
* Shared Storage Volumes,
* Shared Process Namespace. 

To our example we should use the Shared Network namespace, where containers can communicate with each other on the localhost. For instance, container web listening on ports 8080 and db listening 8081. Web container can talk to DB container on localhost:8080.


*Sources*


https://cloudify.co/blog/docker-vs-kubernetes-comparison/

https://www.dynatrace.com/news/blog/kubernetes-vs-docker/

https://k21academy.com/docker-kubernetes/multi-container-pods/

https://azure.microsoft.com/pt-pt/topic/kubernetes-vs-docker/

